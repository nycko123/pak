**Contributors**
- pavbaranov [gitlab](https://gitlab.com/pavbaranov) [github](https://github.com/pavbaranov)
- lami07 [gitlab](https://gitlab.com/lami07)
- LinGruby [gitlab](https://gitlab.com/LinGruby) [github](https://github.com/lingruby-pl)

*~~freenode~~ libera/OFTC IRC - #archlinux.org.pl, #polaur*
- slawul
- robson1
- arch_jest_super

**Packages/tools used**
- [pacman](https://www.archlinux.org/pacman/)
- [pacman-contrib](https://git.archlinux.org/pacman-contrib.git/about/)
- [pkgctl (devtools)](https://gitlab.archlinux.org/archlinux/devtools)
- [namcap](https://projects.archlinux.org/namcap.git/)
- [reflector](https://xyne.archlinux.ca/projects/reflector)
- [fzf](https://github.com/junegunn/fzf)
- [auracle-git](https://github.com/falconindy/auracle.git)
- [downgrade](https://github.com/pbrisbin/downgrade)
- [libnotify](https://developer.gnome.org/notification-spec/)

**Usage:** very basic, small part of available options. Usually not up-to-date => man pak/pak.conf
```
 pak action package(s)
```

**action:** pak uses most of the pacman's flags to perform actions on packages (check 'man pacman' for more info). However there are few exceptions that combines pacman with other tools
```
 -Su / update			- updates all packages from repositories + AUR + POLAUR

 -Sy / -Su			- updates installed packages (sudo pacman -Syu)
				- checks for AUR updates (if 'auracle-git' is installed)
				- installs AUR updates using 'git' and 'makepkg'
				- validates PKGBUILD and built package against errors
				  (if 'namcap' is installed)
				- installs POLAUR updates using 'git' and 'makepkg'

 -Sc / clean			removes copies of uninstalled packages (sudo pacman -Sc)
				- and downloaded files of not installed packages from AUR/POLAUR caches

 -SS / search-all		searches for package(s) everywhere
				- in repositories and AUR/POLAUR
				- using 'pacman', 'git' and 'auracle-git'

 -SI / info-all			prints information on given package(s) from everywhere
				- from repositories and AUR/POLAUR
				- using 'pacman', 'git' and 'auracle-git'

-C / check			prints the list of available updates
				- from repositories and AUR/POLAUR
				- using 'checkupdates' and 'auracle-git' (if installed)

 -m / --mirrors			updates the list of mirrors using 'reflector'

 --vcs				prints the list of installed vcs packages
				- and checks if updated versions are available upstream
```

**action - statistics part:**
```
 -L / logs			prints simple statistics for the current month
				- or other months if specified (pak -L date)
				- date format: YYYY-mm

 -Li				prints statistics of install actions for the current day
				- or other day/month/year if specified (pak -Li date)
				- date format: YYYY-mm-dd, YYYY-mm, YYYY

 -Lr				prints statistics of remove actions for the current day
				- or other day/month/year if specified (pak -Lr date)
				- date format: YYYY-mm-dd, YYYY-mm, YYYY

 -Lu				prints statistics of update actions for the current day
				- or other day/month/year if specified (pak -Lu date)
				- date format: YYYY-mm-dd, YYYY-mm, YYYY
```

**action - AUR part:**  list of flags used to manage AUR packages
```
 -SA / install-aur		installs new package(s) from AUR
				- using 'git clone/pull' and 'makepkg' afterwards
				- validates PKGBUILD and built package against errors
				  (if 'namcap' is installed)

 -SsA / search-aur		searches for package(s) in AUR
				- using 'auracle-git'

 -SiA / info-aur		prints information on given AUR package(s)
				- using 'auracle-git'

 -ScA / clean-aur		removes downloaded files of not installed packages from AUR cache
```

**action - download part:**  pak is able to download PKGBUILDs of official packages (asp) and AUR PKGBUILDs (git)
```
 -G / download			downloads PKGBUILDs of packages from repositories
				- using 'asp export'
				- with the ability to choose what repository to download from

 -GA / download-aur		downloads PKGBUILDs of AUR packages
				- using 'git clone/pull'

 -GB / download-build		downloads and manually builds PKGBUILDs of packages from repositories
				- in a similar way as '-GA'
```

**action - POLAUR part:**  pak provides an easy access to POLAUR repositories (git)
```
 -SyP / sync-polaur		synchronizes POLAUR repositories
 -SP / install-polaur		installs new package(s) from POLAUR
				pak -SP repo/package_name
 -SsP / search-polaur		searches for package(s) in POLAUR
 -NP / news-polaur		prints latest news (in polish) from POLAUR
```

**examples:**
```
 pak -S foo			install 'foo' from repositories (sudo pacman -Syu foo)
 pak -SA bar			install 'bar' from AUR
 pak -SsA foo editor		search 'foo editor' in AUR (auracle search foo editor)
 pak -SiA foo bar		show info about 'foo' and 'bar' from AUR (auracle info foo bar)
 pak -GA foo bar		downloads PKGBUILDs of 'foo' and 'bar' from AUR
```

**full documentation:**
```
 man pak(8), pak.conf(5)
```

**Default configuration file:** (/etc/xdg/pak.conf) can be overwritten by copying above file to $HOME/.config/pak/pak.conf
