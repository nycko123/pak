_pak() {
	local cur prev names opts flags
	COMPREPLY=()
	cur="${COMP_WORDS[COMP_CWORD]}"
	prev="${COMP_WORDS[COMP_CWORD-1]}"
	names="update update-aur install install-aur search search-aur info info-aur search-all info-all download download-fzf download-build download-aur clean clean-aur help version vcs mirrors check logs list-local list-remote list-by-repos sync-polaur install-polaur search-polaur news-polaur update-polaur download-polaur rev-deps list-polaur news-arch infopb-aur info-polaur infopb-polaur news-polaur clean-polaur update-all"
	opts="-Syu -S -SA -SsA -SiA -SS -SI -G -GF -GB -GA -Sc -ScA -h -v -vcs -m -C -L -Li -Lr -Lu -Ld -Ls -SyP -SP -SsP -NP -SyuP -SuyP -GP -d -rd -Pl -SiAp -SiP -SiPp -NA -SyuA -ScP -Sll -SU"
	flags="--version --vcs --mirrors --list-local --list-remote --list-by-repos --downgrade --list-polaur"
	polaur_repos="aur-rebased debug highest-experimental new-branded pkg-trunk recompilated repo-refreshed"
	polaur_repos_slash="aur-rebased/ debug/ highest-experimental/ new-branded/ pkg-trunk/ recompilated/ repo-refreshed/"

	if [[ ${cur} =~ -- ]] ; then
        	COMPREPLY=( $(compgen -W "${flags}" -- ${cur}) )
	elif [[ ${cur} =~ - ]] ; then
		COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
	else
		COMPREPLY=( $(compgen -W "${names}" -- ${cur}) )
	fi

	#case ${prev} in
	#	-SP) COMPREPLY=( $(compgen -W "${polaur_repos}" -- ${cur})/) ;;
	#esac

	if [[ ${prev} =~ -SP ]] || [[ ${prev} =~ -SiP ]]; then
		complete -o nospace -F _pak pak
		COMPREPLY=( $(compgen -o default -W "${polaur_repos_slash}" -- ${cur}) )
	elif [[ ${prev} =~ -Pl ]]; then
		COMPREPLY=( $(compgen -W "${polaur_repos}" -- ${cur}) )
	elif [[ ${prev} =~ - ]]; then
 		COMPREPLY=( $( compgen -o default -o plusdirs  -f -- $cur ) )
	fi
}

complete -F _pak pak
