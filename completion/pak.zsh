#compdef _pak pak

function _pak {
	local state
	typeset -A opt_args

	_arguments -C \
		"1: :->names" \
		"-Syu[Update packages]:" \
		"-S[Install packages]:" \
		"-SA[Install AUR packages]:" \
		"-Ss[Search for packages in repositories]:" \
		"-SsA[Search for packages in the AUR]:" \
		"-Si[Description of packages from repositories]:" \
		"-SiA[Description of packages from the AUR]:" \
		"-SiAp[PKGBUILD of package from the AUR]:" \
		"-SS[Search for packages in repositories and the AUR]:" \
		"-SI[Description of packages from repositories and the AUR]:" \
		"-G[Download PKGBUILDs from repositories]:" \
		"-GF[Download PKGBUILDs from repositories with fzf]:" \
		"-GB[Download PKGBUILDs from repositories and build a package]:" \
		"-GA[Download PKGBUILDs from the AUR]:" \
		"-Sc[Clean cache directories of pacman and pak]:" \
		"-ScA[Clean cache directories of AUR]:" \
		"-ScP[Clean cache directories of POLAUR]:" \
		"-h[Show help information]:" \
		"--help[Show help information]:" \
		"-v[Show versions of pak and pacman]:" \
		"--version[Show versions of pak and pacman]:" \
		"--vcs[Show installed VCS packages]:" \
		"-m[Configure the list of mirrors]:" \
		"--mirrors[Configure the list of mirrors]:" \
		"-C[Show the list of updates]:" \
		"-L[Show information from pacman.log]:" \
		"-Li[Show information from pacman.log - installed]:" \
		"-Lu[Show information from pacman.log - updated]:" \
		"-Ld[Show information from pacman.log - downgraded]:" \
		"-Lr[Show information from pacman.log - removed]:" \
		"-Ls[Show information from pacman.log with given package_name]:" \
		"--list-local[Show full info about installed packages]:" \
		"--list-remote[Show full info about remote packages]:" \
		"--list-by-repos[List packages installed from repositories]:" \
		"-Sll[List packages installed from repositories]:" \
		"-SP[Install POLAUR package]: :->polaur_repos_slash" \
		"-SyP[Sync POLAUR repositories]:" \
		"-SsP[Search for packages in POLAUR]:" \
		"-NP[Latest news from POLAUR RSS feed]:" \
		"-NA[Latest news from Arch Linux]:" \
		"-Pl[List of available packages from POLAUR repositories]: :->polaur_repos" \
		"--list-polaur[List of available packages from POLAUR repositories]:" \
		"-SiP[Description of packages from POLAUR]: :->polaur_repos_slash" \
		"-SiPp[PKGBUILD of packages from POLAUR]: :->polaur_repos_slash" \
		"-SU[Update packages from everywhere]:" \
		"-SyuP[Update packages from POLAUR]:" \
		"-SyuA[Update packages from AUR]:" \
		"-GP[Download PKGBUILDs from POLAUR]:" \
		"--downgrade[downgrade package to lower version]:" \
		"-d[downgrade package to lower version]:" \
		"-rd[Show packages that depend on the named packages]:" \
		"*:arg:->files"

	case $state in
		names) _names ;;
		files)
			compset -P '*,'
			compset -S ',*'
			_files ;;
		polaur_repos)
			compset -P '*,'
			compset -S ',*'
			_polaur_repos ;;
		polaur_repos_slash)
			compset -P '*,'
			compset -S ',*'
			_polaur_repos_slash ;;

#		-L | -Li | -Lu | -Lr | -Ls) _logs ;;
 	esac
}


function _polaur_repos_slash {
	local -a repos
	repos=( \
		"aur-rebased/" \
		"debug/" \
		"highest-experimental/" \
		"new-branded/" \
		"pkg-trunk/" \
		"recompilated/" \
		"repo-refreshed/" \
		)
	compadd -S "" -a repos
	#_describe 'command' repos
}

function _polaur_repos {
	local -a repos
	repos=( \
		"aur-rebased" \
		"debug" \
		"highest-experimental" \
		"new-branded" \
		"pkg-trunk" \
		"recompilated" \
		"repo-refreshed" \
		)
	compadd -S "" -a repos
	#_describe 'command' repos
}

function _names {
	local -a names
	names=( \
		"update:Update packages" \
		"update-all:Update packages from everywhere" \
		"update-aur:Update AUR packages" \
		"install:Install packages" \
		"install-aur:Install AUR packages" \
		"search:Search for packages in repositories" \
		"search-aur:Search for packages in the AUR" \
		"info:Description of packages from repositories" \
		"info-aur:Description of packages from the AUR" \
		"infopb-aur:PKGBUILD of package from the AUR" \
		"search-all:Search for packages in repositories and the AUR" \
		"info-all:Description of packages from repositories and the AUR" \
		"download:Download PKGBUILDs from repositories" \
		"download-fzf:Download PKGBUILDs from repositories with fzf" \
		"download-build:Download PKGBUILDs from repositories and build a package" \
		"download-aur:Download PKGBUILDs from the AUR" \
		"clean:Clean cache directories of pacman and pak" \
		"clean-aur:Clean cache directories of AUR" \
		"clean-polaur:Clean cache directories of POLAUR" \
		"help:Show help information" \
		"version:Show versions of pak and pacman" \
		"vcs:Show installed VCS packages" \
		"mirrors:Configure the list of mirrors" \
		"check:Show the list of updates" \
		"logs:Show short summary from pacman.log" \
		"list-local:Show full info about installed packages" \
		"list-remote:Show full info about remote packages" \
		"list-by-repos:List packages installed from repositories" \
		"sync-polaur:Sync POLAUR repositories" \
		"install-polaur:Install POLAUR package" \
		"search-polaur:Search for packages in POLAUR" \
		"news-polaur:Show latest news from POLAUR RSS feed" \
		"info-polaur:Description of packages from the POLAUR" \
		"infopb-polaur:PKGBUILD of packages from the POLAUR" \
		"update-polaur:Update packages from POLAUR" \
		"list-polaur:List of available packages from POLAUR repositories" \
		"download-polaur:Download PKGBUILDs from POLAUR" \
		"rev-deps:Show packages that depend on the named packages" \
		"news-arch:Show latest Arch Linux news" \
		)
	_describe 'command' names

#	_alternative \
#		'args:custom arg:((a\:"opis a" "b\:"opis b))' \
#		'files:filename:_files'

#	local state
#	typeset -A opt_args
#	_arguments -s \
#		"update[Update packages]:" \
#		"install[Install packages]:" \
#		"install-aur[Install AUR packages]:" \
#		"search[Search for packages in repositories]:" \
#		"search-aur[Search for packages in the AUR]:" \
#		"info[Description of packages from repositories]:" \
#		"info-aur[Description of packages from the AUR]:" \
#		"search-all[Search for packages in repositories and the AUR]:" \
#		"info-all[Description of packages from repositories and the AUR]:" \
#		"download[Download PKGBUILDs from repositories]:" \
#		"download-build[Download PKGBUILDs from repositories and build a package]:" \
#		"download-aur[Download PKGBUILDs from the AUR]:" \
#		"clean[Clean cache directories of pacman and pak]:" \
#		"clean-aur[Clean cache directories of pak]:" \
#		"help[Show help information]:" \
#		"version[Show versions of pak and pacman]:" \
#		"vcs[Show installed VCS packages]:" \
#		"mirrors[Configure the list of mirrors]:" \
#		"check[Show the list of updates]:" \
#		"logs[Show short summary from pacman.log]:"
}

#function _args {
#	return 1
#}

#function _logs {
#	return 0
#}
